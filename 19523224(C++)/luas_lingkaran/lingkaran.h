#ifndef LINGKARAN_H
#define LINGKARAN_H

#include <iostream>
using namespace std;

void bacadataluaslingkaran(float &phi, float &r);
float hitungluaslingkaran(float phi, float r);
void tulisluaslingkaran (float luas);

#endif