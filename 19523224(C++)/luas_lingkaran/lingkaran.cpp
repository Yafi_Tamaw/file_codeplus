#include <iostream>
#include "lingkaran.h"
using namespace std;

void bacadataluaslingkaran(float &phi, float &r){
	cout << "Masukan nilai phi : ";
	cin >> phi;
	cout << "Masukan nilai r : ";
	cin >> r;
}
float hitungluaslingkaran(float phi, float r){
	float luas = phi*r*r;
	return luas;
}
void tulisluaslingkaran (float luas){
	cout << "Luas lingkarannya adalah : " << luas << endl;
}